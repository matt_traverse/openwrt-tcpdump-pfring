# PF_RING based tcpdump for OpenWrt

This is a "first pass" attempt at packaging PF_RING
enabled versions of libpcap and tcpdump.

Due to how the PF_RING versions of libpcap and
tcpdump are distributed, some "hacks" are involved
getting them to cross-compile under OpenWrt's buildroot.

Notes:
- pfring package has been modified to install libpfring.a

  You will need to make sure pfring is installed from _this_
  feed until this change is upstreamed

- tcpdump-pfring is a static binary with static
  libpcap-pfring and pfring compiled in.

- The text "(with PF_RING)" has been added to libpcap-pfring
  version string to differentiate from the system version:
  ```
  libpcap version 1.9.1 (with PF_RING) (with TPACKET_V3)
  ```

Discussion:

https://forum.traverse.com.au/t/tcpdump-dropped-packets/607/4

